<?php include('resources/includes/head.inc.php');?>

<body>
    <?php include('resources/includes/nav.inc.php');
    echo "\n";?>
    <section class="error">
        <h1>Vous devez être connecté pour accéder à cette page</h1>
    </section>
    <footer class="footerError">
        <p>© 2019 - Tous droits réservés - Goût Jeux</p>
    </footer>
</body>

</html>