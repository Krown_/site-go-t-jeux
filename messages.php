<?php include('resources/includes/head.inc.php');?>

<body>
    <?php include('resources/includes/nav.inc.php');
    echo "\n";?>
    <section class="content">
        <?php include('resources/includes/messages.inc.php');?>
    </section>
    <footer class="footer">
        <p>© 2019 - Tous droits réservés - Goût Jeux</p>
    </footer>
</body>

</html>