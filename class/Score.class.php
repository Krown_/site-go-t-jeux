<?php
require_once('MyPDO.class.php');

class Score {
	private $idUt; //int
	private $idJeu; //int
	private $date; //string
	private $val; //int
	private $type; //string

	private function __construct() {}

	/**
	 *Méthode donnant un tableau de tous les scores pour un couple Jeu/typeScore de l'utilisateur donné trié par score puis par date (du plus récent au plus ancien)
	 *
	 *@param $idUt identifiant de l'utilisateur dont on veut le tableau de score (int)
	 *@param $idJeu identifiant du jeu dont on veut le tableau de score (int)
	 *@param $intTpScore type de score (string)
	 *@return tableau de Score (array)
	*/
	public static function getFromUtilisateurJeu(int $idUt, int $idJeu, string $intTpScore) : array {
        
$requete = <<<SQL
		SELECT  s.idUt,
				s.idJeu,
				s.dateScore as "date",
				s.valScore as "val",
				t.intTpScore as "type"
		FROM Score s, TypeScore t
		WHERE s.idTpScore = t.idTpScore
		  AND idUt = :idUt
		  AND idJeu = :idJeu
		  AND t.intTpScore = :intTpScore
		ORDER BY 
SQL;
		if($intTpScore == 's-' || $intTpScore == 'm-') {
			$requete .= 'valScore, dateScore DESC';
		} else {
			$requete .= 'valScore DESC, dateScore DESC';
		}
		$stmt = MyPDO::getInstance()->prepare($requete);
        $stmt->setFetchMode(PDO::FETCH_CLASS, __CLASS__);
        $stmt->execute(array('idUt' => $idUt, 'idJeu' => $idJeu, 'intTpScore' => $intTpScore));

        return $stmt->fetchAll();
	}

	/**
	 *Méthode donnant un tableau de tous les scores pour un couple Jeu/typeScore trié par score puis par date (du plus ancien au plus récent)
	 *
	 *@param $idJeu identifiant du jeu dont on veut le tableau de score (int)
	 *@param $intTpScore type de score (string)
	 *@return tableau de Score (array)
	*/
	public static function getTabScore(int $idJeu, string $intTpScore) : array {
        
$requete = <<<SQL
		SELECT  s.idUt,
				s.idJeu,
				s.dateScore as "date",
				s.valScore as "val",
				t.intTpScore as "type"
		FROM Score s, TypeScore t
		WHERE s.idTpScore = t.idTpScore
		  AND idJeu = :idJeu
		  AND t.intTpScore = :intTpScore
		ORDER BY 
SQL;
		if($intTpScore == 's-' || $intTpScore == 'm-') {
			$requete .= 'valScore, dateScore';
		} else {
			$requete .= 'valScore DESC, dateScore';
		}
		$stmt = MyPDO::getInstance()->prepare($requete);
        $stmt->setFetchMode(PDO::FETCH_CLASS, __CLASS__);
        $stmt->execute(array('idJeu' => $idJeu, 'intTpScore' => $intTpScore));

        return $stmt->fetchAll();
	}

	/**
	 *Méthode retournant tous les meilleurs scores de l'utilisateur pour chaque Jeu/typeScore
	 *
	 *@param $idUt identifiant de l'utilisateur (int)
	 *@return tableau de Score (array)
	*/
	public static function getFromUtilisateur(int $idUt) : array {
		$retour = array();

		$requeteIntTpScore = MyPDO::getInstance()->prepare(<<<SQL
	SELECT intTpScore FROM TypeScore
SQL
);
        $requeteIntTpScore->execute();
		$typeScore = $requeteIntTpScore->fetchAll();

		$requeteNormal = MyPDO::getInstance()->prepare(<<<SQL
SELECT s.idUt,
	   s.idJeu,
	   s.dateScore as "date",
	   s.valScore as "val",
	   t.intTpScore as "type"
  FROM Score s, TypeScore t
  WHERE s.idTpScore = t.idTpScore
    AND t.intTpScore = :intTpScore
	AND s.idUt = :idUt
	AND s.dateScore = (SELECT MIN(s2.dateScore) FROM Score s2
						WHERE s2.idUt = s.idUt
						  AND s2.idJeu = s.idJeu
						  AND s2.idTpScore = s.idTpScore
						  AND s2.valScore = (SELECT MAX(s3.valScore) FROM Score s3
											  WHERE s3.idUt = s.idUt
										        AND s3.idJeu = s.idJeu
												AND s3.idTpScore = s.idTpScore))
SQL
);
        $requeteNormal->setFetchMode(PDO::FETCH_CLASS, __CLASS__);

		$requeteReverse = MyPDO::getInstance()->prepare(<<<SQL
SELECT s.idUt,
	   s.idJeu,
	   s.dateScore as "date",
	   s.valScore as "val",
	   t.intTpScore as "type"
  FROM Score s, TypeScore t
  WHERE s.idTpScore = t.idTpScore
    AND t.intTpScore = :intTpScore
	AND s.idUt = :idUt
	AND s.dateScore = (SELECT MIN(s2.dateScore) FROM Score s2
						WHERE s2.idUt = s.idUt
						  AND s2.idJeu = s.idJeu
						  AND s2.idTpScore = s.idTpScore
						  AND s2.valScore = (SELECT MIN(s3.valScore) FROM Score s3
											  WHERE s3.idUt = s.idUt
										        AND s3.idJeu = s.idJeu
												AND s3.idTpScore = s.idTpScore))
SQL
);
        $requeteReverse->setFetchMode(PDO::FETCH_CLASS, __CLASS__);

		foreach($typeScore as $tS) {
			if($tS["intTpScore"] == 's-' || $tS["intTpScore"] == 'm-') {
				$requeteReverse->execute(array('intTpScore' => $tS["intTpScore"], 'idUt' => $idUt));
				$retour = array_merge($retour, $requeteReverse->fetchAll());
			} else {
				$requeteNormal->execute(array('intTpScore' => $tS["intTpScore"], 'idUt' => $idUt));
				$retour = array_merge($retour, $requeteNormal->fetchAll());
			}
		}
        return $retour;
	}

	/**
	 *Acceseur sur l'identifiant de l'Utilisateur ayant effetué le score
	 *
	 *@return identifiant de l'utilisateur (int)
	*/
	public function getIdUt() : int {
		return $this->idUt;
	}

	/**
	 *Acceseur sur l'identifiant du jeu concerné par le score
	 *
	 *@return identifiant du jeu (int)
	*/
	public function getIdJeu() : int {
		return $this->idJeu;
	}

	/**
	 *Acceseur sur la date du score
	 *
	 *@return date du score (string)
	*/
	public function getDate() : string {
		return $this->date;
	}

	/**
	 *Acceseur sur la valeur du score
	 *
	 *@return valeur du score (string)
	*/
	public function getVal() : string {
		$retour = "";
		switch($this->type) {
			case "score":
				$retour = (string)$this->val;
				break;
			case "s+":
				$retour = (string)floor($this->val/60).":".(string)($this->val%60);
				break;
			case "s-":
				$retour = (string)floor($this->val/60).":".(string)($this->val%60);
				break;
			case "m+":
				$m = floor($this->val/60000);
				$retour = (string)$m;
				$s = floor(($this->val-($m*60000))/1000);
				$retour .= ":$s:".(string)($this->val-($m*60000)-($s*1000));
				break;
			case "m-":
				$m = floor($this->val/60000);
				$retour = (string)$m;
				$s = floor(($this->val-($m*60000))/1000);
				$retour .= ":$s:".(string)($this->val-($m*60000)-($s*1000));
				break;
		}
		return $retour;
	}

	/**
	 *Acceseur sur le type de score
	 *
	 *@return type du score (string)
	*/
	public function getType() : string {
		return $this->type;
	}
}