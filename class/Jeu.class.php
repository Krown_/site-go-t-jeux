<?php
require_once "MyPDO.class.php";
require_once "Score.class.php";

Class Jeu{
    private $idJeu = NULL; //int
    private $nomJeu = NULL; //string
    private $descJeu = NULL; //string
    private $banniereJeu = NULL; //string
    private $jeu = NULL; //string


    /**
	 *Méthode permettant d'instancier un Jeu à partir d'un identifiant
	 *
	 *@param $id identifiant du jeu dont on veut créer l'instance (int)
	 *@return self (Jeu)
	*/
    public static function createFromId(int $id) : self{
        $requete = <<<SQL
            SELECT *
            FROM Jeu
            WHERE idJeu = :idJeu
SQL;
        $stmt = MyPDO::getInstance()->prepare($requete);
        $stmt->setFetchMode(PDO::FETCH_CLASS, __CLASS__);
        $stmt->execute(array('idJeu' => $id));
        if (($object = $stmt->fetch()) !== false) {
            return $object;
        }
        else {
            throw new Exception('Jeu non trouvé');
        }
    }

	/**
	 *Acceseur sur l'identifiant de l'instance de Jeu 
	 *
	 *@return identifiant du Jeu (int)
	*/
    public function getId() : int{
        return $this->idJeu;
    }

	/**
	 *Acceseur sur le nom de l'instance de Jeu 
	 *
	 *@return nom du Jeu (string)
	*/
    public function getNom() : string{
        return $this->nomJeu;
    }

	/**
	 *Acceseur sur la description de l'instance de Jeu 
	 *
	 *@return description du Jeu (string)
	*/
    public function getDescription() : string{
        return $this->descJeu;
    }

    /**
	 *Acceseur sur la banniere d'une instance de Jeu 
	 *
	 *@return la banniere du Jeu (string)
	*/
    public function getBanniereJeu() : string{
        return $this->banniereJeu;
    }

	/**
	 *Méthode permettant de trouver les jeux ayant au moins l'un des types passés en paramètres
	 *
	 *@param $typesJeux types des jeux (array)
	 *@return tableau des jeux (array)
	*/
    public static function searchJeuFromType(array $typesJeux) : array{
        $res = array();
        $requete = <<<SQL
            SELECT *
            FROM `Jeu` 
SQL;
            for ($i = 0; $i < count($typesJeux) ; $i++) { 
                if ($i == 0) {
                    $requete .= <<<SQL
                        WHERE idJeu IN (SELECT idJeu 
                                        FROM `Avoir` 
                                        WHERE idTpJeu IN (SELECT idTpJeu
                                                          FROM `TypeJeu`
                                                          WHERE intTpJeu = '$typesJeux[$i]'))
SQL;
                }
                else {
                    $requete .= <<<SQL
                        OR idJeu IN (SELECT idJeu 
                                      FROM `Avoir` 
                                      WHERE idTpJeu IN (SELECT idTpJeu
                                                        FROM `TypeJeu`
                                                        WHERE intTpJeu = '$typesJeux[$i]'))
SQL;
                }
            } 
        $stmt = MyPDO::getInstance()->prepare($requete); 
        $stmt->setFetchMode(PDO::FETCH_CLASS, __CLASS__);
        $stmt->execute();
        return $stmt->fetchAll();
    }
	/**
	 *Méthode retournant la moyenne de l'instance du Jeu
	 *
	 *@return moyenne des Score (float)
	*/
    public function getNote() : float{
        $requete = <<<SQL
            SELECT ROUND(AVG(note),2) AS moyenne
            FROM Note
            WHERE idJeu = :idJeu;
SQL;
        $stmt = MyPDO::getInstance()->prepare($requete);
        $stmt->execute(array('idJeu' => $this->idJeu));
        $res = $stmt->fetch();
        if (is_null($res['moyenne'])){
            throw new Exception("Pas encore de note sur pour ce jeu");  
        }
        else {
            return $res['moyenne'];        
        }
    }

	/**
	 *Méthode retournant la liste des types de scores accessibles pour ce jeu
	 *
	 *@return liste des types de scores (array)
	*/
    public function getTypeScore() : array{
        $res = array();
        $requete = <<<SQL
            SELECT intTpScore AS nbTypeScore
            FROM TypeScore
            WHERE idTpScore IN( SELECT DISTINCT idTpScore
                                FROM Score
                                WHERE idJeu = :idJeu)
SQL;
        $stmt = MyPDO::getInstance()->prepare($requete); 
        $stmt->execute(array('idJeu' => $this->idJeu));
        $result = $stmt->fetchAll();
        foreach ($result as $typeScore) {
            $res[] = $typeScore['nbTypeScore'];
        }      
        if (is_null($res)){
            throw new Exception("Pas encore de type de score pour ce jeu");  
        }
        else {
            return $res;        
        }
    }

	/**
	 *Méthode retournant la liste des scores du jeu dont le type de score est donnée en paramètre
	 *
     *@param $typeScore le type de scores (string)
     *
	 *@return liste des scores de l'instance Jeu (array)
	*/
    public function getTabScore(string $intTypeScore) : array{
        return Score::getTabScore($this->idJeu, $intTypeScore);
    }
	/**
	 *Méthode retournant le lien vers le code du jeu
	 *
	 *@return lien du jeu (string)
	*/
    public function getJeu() : string{
        return $this->jeu;
    }
}