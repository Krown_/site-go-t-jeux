<?php
require_once('MyPDO.class.php');

class Message {
	private $idExp; //int
	private $idDest; //int
	private $dateMess; //string
	private $message; //string
	private $objet; //string

	/**
	 *Méthode donnant un tableau des messages reçus par l'utilisateur du plus récent au plus ancien
	 *
	 *@param idDest identifiant du destinataire des messages (int)
	 *@return tableau de Message (array)
	*/
	public static function createFromDestinataire(int $idDest) : array {
        $stmt = MyPDO::getInstance()->prepare(<<<SQL
            SELECT *
            FROM Message
			WHERE idDest = :idDest
            ORDER BY dateMess DESC
SQL
        );
        $stmt->setFetchMode(PDO::FETCH_CLASS, __CLASS__);
        $stmt->execute(array('idDest' => $idDest));

        return $stmt->fetchAll();
	}

	/**
	 *Méthode donnant un tableau des messages envoyés par l'utilisateur du plus récent au plus ancien
	 *
	 *@param idexp identifiant de l'expéditeur des messages (int)
	 *@return tableau de Message (array)
	*/
	public static function createFromExpediteur(int $idExp) : array {
        $stmt = MyPDO::getInstance()->prepare(<<<SQL
            SELECT *
            FROM Message
			WHERE idExp = :idExp
            ORDER BY dateMess DESC
SQL
        );
        $stmt->setFetchMode(PDO::FETCH_CLASS, __CLASS__);
        $stmt->execute(array('idExp' => $idExp));

        return $stmt->fetchAll();
	}

	private function __construct() {}

	/**
	 *Accesseur sur l'identifiant de l'expéditeur
	 *
	 *@return Identifiant de l'expéditeur (int)
	*/
	public function getIdExp() : int {
		return $this->idExp;
	}

	/**
	 *Accesseur sur l'identifiant du destinataire
	 *
	 *@return Identifiant du destinataire (int)
	*/
	public function getIdDest() : int {
		return $this->idDest;
	}

	/**
	 *Accesseur sur la date du message
	 *
	 *@return Date du message (string)
	*/
	public function getDateMess() : string {
		return $this->dateMess;
	}

	/**
	 *Accesseur sur le contenu du message
	 *
	 *@return Contenu du message (string)
	*/
	public function getMessage() : string {
		return $this->message;
	}

	/**
	 *Accesseur sur l'objet du message
	 *
	 *@return Objet du message (string)
	*/
	public function getObjet() : string {
		return $this->objet;
	}
}