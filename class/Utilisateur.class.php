<?php
require_once('MyPDO.class.php');
require_once('Score.class.php');

class Utilisateur{    
    private $idUt = NULL;
    private $nomUt = NULL;
    private $prenomUt = NULL;
    private $pseudoUt = NULL;
    private $mailUt = NULL;
    private $sexeUt = NULL;
    private $dateNaissUt = NULL;
    private $ppUt = NULL;
    private $afficherNomPrenomUt = NULL;
    private $afficherAgeUt = NULL;
    /**
     * Usine pour fabriquer une instance à partir d'un identifiant.
     *
     * Les données sont issues de la base de données
     *
     * @param int $id identifiant BD de l'utilisateur à créer
     *
     * @throws Exception si l'utilisateur ne peut pas être trouvé dans la base de données
     *
     * @return Utilisateur instance correspondant à $id
     */
    public static function createFromId(int $id):self{
        $stmt = MyPDO::getInstance()->prepare("SELECT idUt,
													   nomUt,
													   prenomUt,
													   pseudoUt,
													   mailUt,
													   sexeUt,
													   dateNaissUt,
													   ppUt,
													   afficherNomPrenomUt,
													   afficherAgeUt
                                               FROM Utilisateur
                                               WHERE idUt = :idUt");
        $stmt->setFetchMode(PDO::FETCH_CLASS, __CLASS__);
        $stmt->execute(array('idUt' => $id));
        if (($object = $stmt->fetch()) !== false) {
            return $object;
        }
        throw new Exception('Utilisateur non trouvé');
    }
    
    /**
     * Usine pour fabriquer une instance à partir d'un pseudo.
     *
     * Les données sont issues de la base de données
     *
     * @param int $pseudo pseudo BD de l'utilisateur à créer
     *
     * @throws Exception si l'utilisateur ne peut pas être trouvé dans la base de données
     *
     * @return Utilisateur instance correspondant à $pseudo
     */
    public static function createFromPseudo(string $pseudo):self{
        $stmt = MyPDO::getInstance()->prepare("SELECT idUt,
													   nomUt,
													   prenomUt,
													   pseudoUt,
													   mailUt,
													   sexeUt,
													   dateNaissUt,
													   ppUt,
													   afficherNomPrenomUt,
													   afficherAgeUt
                                               FROM Utilisateur
                                               WHERE pseudoUt = :pseudoUt");
        $stmt->setFetchMode(PDO::FETCH_CLASS, __CLASS__);
        $stmt->execute(array('pseudoUt' => $pseudo));
        if (($object = $stmt->fetch()) !== false) {
            return $object;
        }
        throw new PDOException('Utilisateur non trouvé');
    } 
    
	/**
	 *Accesseur sur l'identifiant de l'utilisateur
	 *
	 *@return Identifiant de l'expéditeur (int)
	*/
    public function getId():int{
        return $this->idUt;
    }
    	/**
	 *Accesseur sur l'identifiant de l'expéditeur
	 *
	 *@return Nom de l'expéditeur (string)
	*/
    public function getNom():string{
        return $this->nomUt;
    }
    public function getPrenom():string{
        return $this->prenomUt;
    }
    public function getPseudo():string{
        return $this->pseudoUt;
    }
    public function getMail():string{
        return $this->mailUt;
    }
    public function getSexe():string{
        return $this->sexeUt;
    }
    public function getDateNaiss():string{
        return $this->dateNaissUt;
    }
    public function getPp():string{
        return $this->ppUt;
    }
    public function getAfficherNomPrenom():bool{
        return ($this->afficherNomPrenomUt == 0 ? false : true);
    }
    public function getAfficherAge():bool{
        return ($this->afficherAgeUt == 0 ? false : true);
    }

    public function getAge():int{
        $bdd = MyPDO::getInstance()->prepare("SELECT ROUND(DATEDIFF(NOW(),dateNaissUt)/365.25) AS ageUt
                                              FROM Utilisateur 
                                              WHERE idUt = :idUt");
        $bdd->execute(array('idUt' => $this->idUt));
        if (($res = $bdd->fetch()) !== false) {
            return $res['ageUt'];
        }
    }
    public function setPseudo(string $pseudo) {
        $this->pseudoUt = $pseudo;
        $bdd = MyPDO::getInstance()->prepare("UPDATE Utilisateur
                                              SET pseudoUt = :pseudoUt
                                              WHERE idUt = :idUt");
        $bdd->execute(array('pseudoUt' => $pseudo,'idUt' => $this->idUt));
    }

    public function setMail(string $mail) {
        $this->mailUt = $mail;
        $bdd = MyPDO::getInstance()->prepare("UPDATE Utilisateur
                                              SET mailUt = :mailUt
                                              WHERE idUt = :idUt");
        $bdd->execute(array('mailUt' => $mail,'idUt' => $this->idUt));
    }

    public function setPp(string $pp) {
        $this->ppUt = $pp;
        $bdd = MyPDO::getInstance()->prepare("UPDATE Utilisateur
                                              SET ppUt = :ppUt
                                              WHERE idUt = :idUt");
        $bdd->execute(array('ppUt' => $pp,'idUt' => $this->idUt));
    }

    public function setAfficherNomPrenom(int $afficherNomPrenom) {
        $afficherNomPrenom = $afficherNomPrenom;
        if ($afficherNomPrenom == 1 || $afficherNomPrenom == 0){
            $this->afficherNomPrenomUt = $afficherNomPrenom;
            $bdd = MyPDO::getInstance()->prepare("UPDATE Utilisateur
                                                  SET afficherNomPrenomUt = :afficherNomPrenomUt
                                                  WHERE idUt = :idUt");
            $bdd->execute(array('afficherNomPrenomUt' => $afficherNomPrenom,'idUt' => $this->idUt));
        }
    }

    public function setAfficherAge(int $afficherAge) {
        $afficherAge = $afficherAge;
        if ($afficherAge == 1 || $afficherAge == 0){
            $this->afficherAgeUt = $afficherAge;
            $bdd = MyPDO::getInstance()->prepare("UPDATE Utilisateur
                                                  SET afficherAgeUt = :afficherAgeUt
                                                  WHERE idUt = :idUt");
            $bdd->execute(array('afficherAgeUt' => $afficherAge,'idUt' => $this->idUt));
        }
    }
    public function setMdp(string $mdp) {
        $passwd = password_hash($mdp,PASSWORD_DEFAULT);
        $bdd = MyPDO::getInstance()->prepare("UPDATE Utilisateur
                                              SET mdp = :mdp
                                              WHERE idUt = :idUt");
        $bdd->execute(array('mdp' => $passwd,'idUt' => $this->idUt));
    }

    public function sendMessage(Utilisateur $utilisateur, string $objet, string $message) {
        if (isset($utilisateur)) {
            $bdd = MyPDO::getInstance()->prepare("INSERT INTO Message
                                                  VALUES(:idExp,:idDest,NOW(),:message,:objet)");
            $bdd->execute(array('idExp' => $this->idUt,'idDest' => $utilisateur->getId(),':message' => $message, 'objet' => $objet));  
        }
    }

	/**
	 *Méthode donnant les meilleurs scores de l'utilisateur pour chaque Jeu/TypeScore
	 *
	 *@return Tableau de score du joueur (array)
	*/
	public function getScoreTab() : array {
		return Score::getFromUtilisateur($this->idUt);
	}

	/**
	 *Méthode permettant d'enregistrer un score en donnant l'identifiant du jeu, le type de score, et le score
	 *
	 *@param idJeu identifiant du jeu sur lequel le joueur a effectué un score (int)
	 *@param valScore score effectué déjà converti en entier (int)
	 *@param intTpScore type de score (string)
	*/
	public function markScore(int $idJeu, int $valScore, string $intTpScore) : void {
		$req = MyPDO::getInstance()->prepare("INSERT INTO Score
                                                  VALUES(:idUt, :idJeu, NOW(), :valScore, (SELECT idTpScore FROM TypeScore WHERE intTpScore = :intTpScore))");
		try {
			$req->execute(array('idUt' => $this->idUt, 'idJeu' => $idJeu, 'valScore' => $valScore, 'intTpScore' => $intTpScore));
		} catch(PDOException $e) {
		}
	}
}

