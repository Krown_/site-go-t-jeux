<?php include('resources/includes/head.inc.php');?>

<body>
    <?php include('resources/includes/nav.inc.php');
    echo "\n";?>
    <section class="content">
        <div class="wrapper">
            <div class="verifMDP alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                Erreur : Les mots de passe ne correspondent pas 
            </div>
            <div class="verifUser alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                Erreur : Pseudo déjà utilisé 
            </div>
            <div class="verifMail alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                Erreur : Mail déjà utilisé 
            </div>
            <form method="post" action="/resources/scripts/register.php" enctype="multipart/form-data" onsubmit="javascript: return check_password('password_1', 'password_2');">
                <h1>Inscription</h1>
                <div class="form-row pt-2">
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Nom</label>
                        <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last name" required>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="firstName">Prénom</label>
                        <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pseudo">Pseudonyme</label>
                    <input type="text" class="form-control" id="pseudo" name="pseudo" placeholder="Enter pseudo" required>
                    <span></span>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="dateNais">Date de naissance</label>
                        <input type="date" class="form-control" id="dateNais" name="dateNais" required>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="my-1 mr-2" for="sexe">Sexe</label>
                        <select class="custom-select mr-sm-2" id="sexe" name="sexe" required>
                            <option value="" disabled="" selected="">Choisissez...</option>
                            <option value="m">Homme</option>
                            <option value="f">Femme</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
                </div>
                <div class="form-group">
                    <label for="password_1">Mot de passe :</label>
                    <input type="password" class="form-control" id="password_1" name="password_1" placeholder="Enter password" required>
                    <label for="password_2">Vérification :</label>
                    <input type="password" class="form-control" id="password_2" name="password_2" placeholder="Enter password" required>
                </div>
                <div class="text-center">
                    <button type="submit" id="registerSubmit" class="btn btn-purple">S'enregistrer</button>
                </div>
            </form>
        </div>
    </section>
    <footer class="footer">
        <p>© 2019 - Tous droits réservés - Goût Jeux</p>
    </footer>
</body>

</html>