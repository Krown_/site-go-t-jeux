var game;
var gameOptions = {
    tileSize: 200,
    Speed: 10,
    tileSpacing: 20,
}
var ROW = 0;
var COL = 1;
window.onload = function() {
    var gameConfig = {
       type: Phaser.CANVAS,
       width: gameOptions.tileSize * 5 + gameOptions.tileSpacing * 6,
       height: (gameOptions.tileSize * 3.5 + gameOptions.tileSpacing * 6) * 16 / 9,
       backgroundColor: 0xecf0f1,
       scene: [preloadAssets, playGame]
   };
    game = new Phaser.Game(gameConfig);
    window.focus()
    var canvas = document.querySelector("canvas");
    canvas.style.width = 800 + "px";
    canvas.style.height = 800 + "px";
}

var preloadAssets = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize:
    function preloadAssets(){
        Phaser.Scene.call(this, {key: "PreloadAssets"});
    },
    preload: function(){
        this.load.image("spot", "../resources/games/assets/2048/sprites/spot.png");
        this.load.image("restart", "../resources/games/assets/2048/sprites/restart.png");
        this.load.image("scorepanel", "../resources/games/assets/2048/sprites/scorepanel.png");
        this.load.image("scorelabels", "../resources/games/assets/2048/sprites/scorelabels.png");
        this.load.spritesheet("tiles", "../resources/games/assets/2048/sprites/tiles.png", {
            frameWidth: gameOptions.tileSize,
            frameHeight: gameOptions.tileSize
        });
        this.load.bitmapFont("font", "../resources/games/assets/2048/fonts/font.png", "../resources/games/assets/2048/fonts/font.fnt");
    },
    create: function(){
        this.scene.start("PlayGame");
    }
})

var playGame = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize:
    function playGame(){
        Phaser.Scene.call(this, {key: "PlayGame"});
    },
    create: function(){
        this.case = [];
        this.caseGroupe = this.add.group();
        this.score = 0;
        for(var i = 0; i < 5; i++){
            this.case[i] = [];
            for(var j = 0; j < 5; j++){
                var spot = this.add.sprite(this.tileDestination(j, COL), this.tileDestination(i, ROW), "spot")
                var tile = this.add.sprite(this.tileDestination(j, COL), this.tileDestination(i, ROW), "tiles");
                tile.alpha = 0;
                tile.visible = 0;
                this.caseGroupe.add(tile);
                this.case[i][j] = {
                    caseVal: 0,
                    tileSprite: tile,
                    canUpgrade: true
                }
            }
        }
        var restartButton = this.add.sprite(this.tileDestination(4, COL), this.tileDestination(5, ROW), "restart");
        restartButton.setInteractive();
        restartButton.on("pointerdown", function(){
            this.scene.start("PlayGame");
        }, this)
        this.add.sprite(this.tileDestination(1, COL) - 170, this.tileDestination(5, ROW), "scorepanel");
        this.add.sprite(this.tileDestination(0, COL) - 50, this.tileDestination(5, ROW)-70, "scorelabels");
        this.scoreText = this.add.bitmapText(this.tileDestination(0, COL) - 80, this.tileDestination(5, ROW) - 25, "font", "0");
        this.input.keyboard.on("keydown", this.handleKey, this);
        this.canMove = false;
        this.addTile();
        this.addTile();
    },
    addTile: function(){
        var caseVide = [];
        for(var i = 0; i < 5; i++){
            for(var j = 0; j < 5; j++){
                if(this.case[i][j].caseVal == 0){
                    caseVide.push({
                        row: i,
                        col: j
                    })
                }
            }
        }
        if(caseVide.length > 0){
            var currentCase = Phaser.Utils.Array.GetRandomElement(caseVide);
            this.case[currentCase.row][currentCase.col].caseVal = 1;
            this.case[currentCase.row][currentCase.col].tileSprite.visible = true;
            this.case[currentCase.row][currentCase.col].tileSprite.setFrame(0);
            this.tweens.add({
                targets: [this.case[currentCase.row][currentCase.col].tileSprite],
                alpha: 1,
                duration: gameOptions.Speed,
                onComplete: function(tween){
                    tween.parent.scene.canMove = true;
                },
            });
        }
	},
    handleKey: function(e){
        if(this.canMove){
            var childCase = this.caseGroupe.getChildren();
            switch(e.code){
                case "ArrowLeft":
                    for (var i = 0; i < childCase.length; i++){
                        childCase[i].depth = childCase[i].x;
                    }
                    this.handleMove(0, -1);
                    this.verif();
                    break;
                case "ArrowRight":
                    for (var i = 0; i < childCase.length; i++){
                        childCase[i].depth = game.config.width - childCase[i].x;
                    }
                    this.handleMove(0, 1);
                    this.verif();
                    break;
                case "ArrowUp":
                    for (var i = 0; i < childCase.length; i++){
                        childCase[i].depth = childCase[i].y;
                    }
                    this.handleMove(-1, 0);
                    this.verif();
                    break;
                case "ArrowDown":
                    for (var i = 0; i < childCase.length; i++){
                        childCase[i].depth = game.config.height - childCase[i].y;
                    }
                    this.handleMove(1, 0);
                    this.verif();
                    break;
            }
        }
    },
	moveEnd: function(e){
        var moveTime = e.upTime - e.downTime;
        var move = new Phaser.Geom.Point(e.upX - e.downX, e.upY - e.downY);
        var moveMagnitude = Phaser.Geom.Point.GetMagnitude(move);
        var moveNormal = new Phaser.Geom.Point(move.x / moveMagnitude, move.y / moveMagnitude);
        if(moveMagnitude > 20 && moveTime < 1000 && (Math.abs(moveNormal.y) > 0.8 || Math.abs(moveNormal.x) > 0.8)){
            var childCase = this.caseGroupe.getChildren();
            if(moveNormal.x > 0.8) {
                for (var i = 0; i < childCase.length; i++){
                    childCase[i].depth = game.config.width - childCase[i].x;
                }
                this.handleMove(0, 1);
            }
            if(moveNormal.x < -0.8) {
                for (var i = 0; i < childCase.length; i++){
                    childCase[i].depth = childCase[i].x;
                }
                this.handleMove(0, -1);
            }
            if(moveNormal.y > 0.8) {
                for (var i = 0; i < childCase.length; i++){
                    childCase[i].depth = game.config.height - childCase[i].y;
                }
                this.handleMove(1, 0);
            }
            if(moveNormal.y < -0.8) {
                for (var i = 0; i < childCase.length; i++){
                    childCase[i].depth = childCase[i].y;
                }
                this.handleMove(-1, 0);
            }
        }
    },
    moveTile: function(tile, row, col, distance, changeNumber){
        this.movingTiles ++;
        this.tweens.add({
            targets: [tile.tileSprite],
            x: this.tileDestination(col, COL),
            y: this.tileDestination(row, ROW),
            duration: gameOptions.Speed * distance,
            onComplete: function(tween){
                tween.parent.scene.movingTiles --;
                if(changeNumber){
                    tween.parent.scene.transformTile(tile, row, col);
                }
                if(tween.parent.scene.movingTiles == 0){
                    tween.parent.scene.scoreText.text = tween.parent.scene.score.toString();
                    tween.parent.scene.resetTiles();
                    tween.parent.scene.addTile();
                }
            }
        })
    },
    transformTile: function(tile, row, col){
        this.movingTiles ++;
        tile.tileSprite.setFrame(this.case[row][col].caseVal - 1);
        this.tweens.add({
            targets: [tile.tileSprite],
            scaleX: 1.1,
            scaleY: 1.1,
            duration: gameOptions.Speed,
            yoyo: true,
            repeat: 1,
            onComplete: function(tween){
                tween.parent.scene.movingTiles --;
                if(tween.parent.scene.movingTiles == 0){
                    tween.parent.scene.scoreText.text = tween.parent.scene.score.toString();
                    tween.parent.scene.resetTiles();
                    tween.parent.scene.addTile();
                }
            }
        })
    },
    resetTiles: function(){
        for(var i = 0; i < 5; i++){
            for(var j = 0; j < 5; j++){
                this.case[i][j].canUpgrade = true;
                this.case[i][j].tileSprite.x = this.tileDestination(j, COL);
                this.case[i][j].tileSprite.y = this.tileDestination(i, ROW);
                if(this.case[i][j].caseVal > 0){
                    this.case[i][j].tileSprite.alpha = 1;
                    this.case[i][j].tileSprite.visible = true;
                    this.case[i][j].tileSprite.setFrame(this.case[i][j].caseVal - 1);
                }
                else{
                    this.case[i][j].tileSprite.alpha = 0;
                    this.case[i][j].tileSprite.visible = false;
                }
            }
        }
    },
    isInsideBoard: function(row, col){
        return (row >= 0) && (col >= 0) && (row < 5) && (col < 5);
    },
    tileDestination: function(pos, axis){
        var offset = (axis == ROW) ? (game.config.height - game.config.width) / 2 : 0;
        return pos * (gameOptions.tileSize + gameOptions.tileSpacing) + gameOptions.tileSize / 2 + gameOptions.tileSpacing + offset;
    },
	handleMove: function(deltaRow, deltaCol){
        this.canMove = false;
        var hasMoved = false;
        this.movingTiles = 0;
        var moveScore = 0;
        for(var i = 0; i < 5; i++){
            for(var j = 0; j < 5; j++){
                var colToWatch = deltaCol == 1 ? (5 - 1) - j : j;
                var rowToWatch = deltaRow == 1 ? (5 - 1) - i : i;
                var caseVal = this.case[rowToWatch][colToWatch].caseVal;
                if(caseVal != 0){
                    var colSteps = deltaCol;
                    var rowSteps = deltaRow;
                    while(this.isInsideBoard(rowToWatch + rowSteps, colToWatch + colSteps) && this.case[rowToWatch + rowSteps][colToWatch + colSteps].caseVal == 0){
                        colSteps += deltaCol;
                        rowSteps += deltaRow;
                    }
                    if(this.isInsideBoard(rowToWatch + rowSteps, colToWatch + colSteps) && (this.case[rowToWatch + rowSteps][colToWatch + colSteps].caseVal == caseVal) && this.case[rowToWatch + rowSteps][colToWatch + colSteps].canUpgrade && this.case[rowToWatch][colToWatch].canUpgrade && caseVal < 12){
                        this.case[rowToWatch + rowSteps][colToWatch + colSteps].caseVal ++;
                        moveScore += Math.pow(2, this.case[rowToWatch + rowSteps][colToWatch + colSteps].caseVal);
                        this.case[rowToWatch + rowSteps][colToWatch + colSteps].canUpgrade = false;
                        this.case[rowToWatch][colToWatch].caseVal = 0;
                        this.moveTile(this.case[rowToWatch][colToWatch], rowToWatch + rowSteps, colToWatch + colSteps, Math.abs(rowSteps + colSteps), true);
                        hasMoved = true;
                    }
                    else{
                        colSteps = colSteps - deltaCol;
                        rowSteps = rowSteps - deltaRow;
                        if(colSteps != 0 || rowSteps != 0){
                            this.case[rowToWatch + rowSteps][colToWatch + colSteps].caseVal = caseVal;
                            this.case[rowToWatch][colToWatch].caseVal = 0;
                            this.moveTile(this.case[rowToWatch][colToWatch], rowToWatch + rowSteps, colToWatch + colSteps, Math.abs(rowSteps + colSteps), false);
                            hasMoved = true;
                        }
                    }
                }
            }
        }
        if(!hasMoved){
            this.canMove = true;
        }
        else{
            this.score += moveScore;
        }
    },
    verif: function(){
        var end = true;
        for(var i = 0; i < 5; i++){
            for(var j = 0; j < 5; j++){
                if(this.case[i][j].caseVal == 0){
                    end = false;
                }
            }
        }
        if(end != false) {
            appendScore(this.score,"score");
            alert("partie terminée");
            this.scene.start("PlayGame");
        }
    }
});
