var hauteur = 600;
    var largeur = 800;

    var config = {
        type: Phaser.AUTO,
        width: largeur,
        height: hauteur,
        physics: {
            default: 'arcade',
            arcade: {
                debug: false
                }
        },
        scene: {
            preload: preload,
            create: create,
            update: update
        },
        audio: {
            disableWebAudio: true
        }
    };
    

    var game = new Phaser.Game(config);
    var score ; //int ; contient le score actuel
    var etat;  //0 : depart  ; 1 : en jeu  ; 2 : game over
    var entree; //Touche entree
    var spacebar; //touche espace
    var boucle; //contient la boucle qui génère les vagues
    var lasers; //groupe lasers
    var txt; //texte de droite
    var scoreText; //affichage du score
    var plateformes; //groupe plateformes
    var music;
    var premierepartie = true;

    
    
    function preload()
    {
        this.load.image('background','../resources/games/assets/flappygravity/background.png');
        this.load.image('plateforme','../resources/games/assets/flappygravity/platform.png');
        this.load.image('laser','../resources/games/assets/flappygravity/laser.png');
        this.load.image('vie','../resources/games/assets/flappygravity/vie.png');
        this.load.image('gold','../resources/games/assets/flappygravity/gold.png');
        this.load.spritesheet('perso', 
            '../resources/games/assets/flappygravity/perso.png',
            { frameWidth: 30, frameHeight: 30 }
        );
        this.load.audio('music','../resources/games/assets/flappygravity/music.mp3');
        
    }


    function create()
    {
        //musique
        music = game.sound.add('music',{volume: 0.05, loop : true});
        if (premierepartie)
        {
            music.play();
        }

        //Setup du clavier
        cursors = this.input.keyboard.createCursorKeys();
        entree = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ENTER);
        spacebar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);


        //setup background
        this.add.image(largeur/2,hauteur/2,'background');


        //setup plateforme
        plateformes = this.physics.add.staticGroup();
        plateformes.create(largeur/2 ,hauteur,'plateforme');
        

        //Setup des textes / score
        score = 0;        
        scoreText = this.add.text(50, 535, 'score: 0', { fontSize: '32px', fill: '#000' });


        //setup joueur
        player = this.physics.add.sprite(100, 400, 'perso');
        player.body.gravity.y = 600;


        //animations de retournement
        this.anims.create({
            key: 'normal',
            frames: [ { key: 'perso', frame: 0 } ],
            frameRate: 20
        });

        this.anims.create({
            key: 'inverse',
            frames: [ { key: 'perso', frame: 1 } ],
            frameRate: 20
        });



        //setup laser
        lasers = this.physics.add.group();


        //setup pièces
        golds = this.physics.add.group();


        //pièce récupérée
        function collectGold() {
            var goldc = golds.getChildren()[0];
            golds.remove(goldc,true,true);
            score++;
            scoreText.setText('Score: ' + score);
        }
        

        //setup contacts
        player.setCollideWorldBounds(true);
        this.physics.add.collider(player,plateformes);
        this.physics.add.overlap(player, lasers, gameover, null, this);
        this.physics.add.overlap(player, golds, collectGold, null, this);


        //game over
        function gameover() {
            this.physics.pause();
            player.setTint(0xff0000);
            boucle.remove(false);
            txt = this.add.text(350, 540, 'Appuie sur Espace pour recommencer !', { fontSize: '16px', fill: '#000' });
            etat = 2;
            premierepartie = false;
			appendScore(score,'score');
        }

        //écran de début
        etat = 0;
        this.physics.pause();
        txt = this.add.text(350, 540, 'Appuie sur Espace pour jouer !', { fontSize: '16px', fill: '#000' });

    }
    

    //fonction pour générer une vague
    function vague() {
            debut = true;            
            var max;
            var min;

            //empêche un espacement trop important entre 2 passages
            if (posi <= 50)
            {
                max == 470 - posi + 350;
            }
            else if (posi >= 450)
            {
                min = 30 + posi - 350;
            }

            //définit la position du passage
            var pos = Phaser.Math.RND.integerInRange(30, 500 - 130)
            var posi = pos;

            //obstacle du haut
            laser1 = lasers.create(largeur,pos/2,'laser');            
            laser1.setVelocityX(-200);
            laser1.setDisplaySize(laser1.width,pos);

            //obstacle du bas
            laser2 = lasers.create(largeur,(500+pos+100)/2,'laser');
            laser2.setVelocityX(-200);
            laser2.setDisplaySize(laser2.width,500 - 100 - pos);

            //golds
            gold = golds.create(largeur,pos+50,'gold');
            gold.setVelocityX(-200);
        }


    function update()
    {
       
        //touche espace
        if (Phaser.Input.Keyboard.JustDown(spacebar))
        {
            if (etat == 1)
            {
                //inversion de la gravité du joueur
                player.body.gravity.y = -(player.body.gravity.y);
                if (player.body.gravity.y == 600)
                {
                    player.anims.play('normal');
                }
                else
                {
                    player.anims.play('inverse',false);
                }
                
            }
            else if (etat == 0)
            {
                //démarrage du jeu
                etat = 1;
                this.physics.resume("default");
                txt.setText("")
                boucle = this.time.addEvent({ delay: 1200, callback: vague, callbackScope: this, loop: true });
            }
            else
            {
                etat = 0;
                this.scene.restart();
            }
            
        }
    }
