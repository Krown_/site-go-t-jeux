var config = {
    type: Phaser.AUTO,
    width: 544,
    height: 383,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);
var vies = 3;
var score = 0;
var recoveryTime = false;
var directionJoueur = 3;
var directionNom = ['left', 'up', 'right', 'down'];
var fin = false;

function preload () {
	this.load.image('sol', '/resources/games/assets/coincollecter/ground.png');
	this.load.image('mur', '/resources/games/assets/coincollecter/wall.png');
	this.load.image('mob', '/resources/games/assets/coincollecter/mob.png');
	this.load.image('endScreen', '/resources/games/assets/coincollecter/fin.png');
	this.load.image('coeur', '/resources/games/assets/coincollecter/coeur.png');
	this.load.image('coeur_vide', '/resources/games/assets/coincollecter/coeur_vide.png');
	this.load.spritesheet('joueur', '/resources/games/assets/coincollecter/player.png', {frameWidth: 32, frameHeight: 32});
	this.load.spritesheet('coin', '/resources/games/assets/coincollecter/coin.png', {frameWidth: 32, frameHeight: 32});
}

function create () {
	//Dessin du sol
	for(var i=1; i<16; i++) {
		for(var j =1; j<11; j++) {
			this.add.image(i*32, j*32, 'sol').setOrigin(0, 0);
		}
	}

	//Création des objets
	murs = this.physics.add.staticGroup();
	player = this.physics.add.sprite(48, 48, 'joueur');
	cursors = this.input.keyboard.createCursorKeys();
	coins = this.physics.add.group({
		setXY: {
			x: Math.floor((Math.random() * 15) + 1)*32+16,
			y: Math.floor((Math.random() * 10) + 1)*32+16
		}
	});
	ghosts = this.physics.add.group({
		bounceX: 1,
		bounceY: 1
	});

	//Ajout des relations
	this.physics.add.collider(player, murs);
	this.physics.add.collider(ghosts, murs);
	this.physics.add.overlap(player, coins, collectCoin, null, this);
	this.physics.add.overlap(player, ghosts, hit, null, this);

	//Les murs
	for(var i=0; i<17; i++) {
		murs.create(32*i+16,16,'mur');
		murs.create(32*i+16,32*11+16,'mur');
	}

	for(var i=1; i<11; i++) {
		murs.create(16, i*32+16, 'mur');
		murs.create(16*32+16, i*32+16, 'mur');
	}

	//Les coeurs
	for(var i=0; i<3; i++) {
		this.add.image(i*32, 0, 'coeur').setOrigin(0, 0);
	}

	//Les animations du joueur
	this.anims.create({
		key: 'left',
		frames: this.anims.generateFrameNumbers('joueur', { start: 0, end: 7}),
		frameRate: 10,
		repeat: -1
	});
	this.anims.create({
		key: 'up',
		frames: this.anims.generateFrameNumbers('joueur', { start: 8, end: 15}),
		frameRate: 10,
		repeat: -1
	});
	this.anims.create({
		key: 'right',
		frames: this.anims.generateFrameNumbers('joueur', { start: 16, end: 23}),
		frameRate: 10,
		repeat: -1
	});
	this.anims.create({
		key: 'down',
		frames: this.anims.generateFrameNumbers('joueur', { start: 24, end: 31}),
		frameRate: 10,
		repeat: -1
	});
	this.anims.create({
		key: 'left_stop',
		frames: [{key: 'joueur', frame: 4}],
		frameRate: 10,
	});
	this.anims.create({
		key: 'up_stop',
		frames: [{key: 'joueur', frame: 12}],
		frameRate: 10,
	});
	this.anims.create({
		key: 'right_stop',
		frames: [{key: 'joueur', frame: 20}],
		frameRate: 10,
	});
	this.anims.create({
		key: 'down_stop',
		frames: [{key: 'joueur', frame: 28}],
		frameRate: 10,
	});

	//L'animation des pièces
	this.anims.create({
		key: 'spark',
		frames: this.anims.generateFrameNumbers('coin', { start: 0, end: 31}),
		frameRate: 10,
		repeat: -1
	});

	//Le premier fantome
	ajouterFantome();
	//La première pièce
    var premierePiece = coins.create(Math.floor((Math.random() * 15) + 1)*32+16, Math.floor((Math.random() * 10) + 1)*32+16, 'coin');
	premierePiece.anims.play('spark', true);
}

function update () {
	if(vies == 0) { //Si la partie est finie
		this.physics.pause();
		player.anims.play(directionNom[directionJoueur]+'_stop', true);
		var endScreen = this.physics.add.image(272, 192+16, 'endScreen');
		var scoreText = this.add.text(225, 227, 'score: '+score, { fontSize: '32px', fill: '#000' });
		if(!fin) {
			appendScore(score, 'score');
			fin = true;
		}
		return;
	}

	//Gestion des déplacements du joueur
	if(cursors.left.isDown) {
		directionJoueur = 0;
		player.setVelocityX(-160);
		player.setVelocityY(0);
		player.anims.play(directionNom[directionJoueur], true);
	} else if (cursors.right.isDown) {
		directionJoueur = 2;
		player.setVelocityX(160);
		player.setVelocityY(0);
		player.anims.play(directionNom[directionJoueur], true);
	} else if(cursors.up.isDown) {
		directionJoueur = 1;
		player.setVelocityY(-160);
		player.setVelocityX(0);
		player.anims.play(directionNom[directionJoueur], true);
	} else if(cursors.down.isDown) {
		directionJoueur = 3;
		player.setVelocityY(160);
		player.setVelocityX(0);
		player.anims.play(directionNom[directionJoueur], true);
	} else {
		player.anims.play(directionNom[directionJoueur]+'_stop', true);
		player.setVelocityX(0);
		player.setVelocityY(0);
	}
}

function collectCoin (player, coin) {
    coin.disableBody(true, true);
	var piece = coins.create(Math.floor((Math.random() * 15) + 1)*32+16, Math.floor((Math.random() * 10) + 1)*32+16, 'coin');
	piece.anims.play('spark', true);

	ajouterFantome();

	score++;
}

function hit(player, ghosts) {
	if(!recoveryTime) {
		vies--;
		recoveryTime = true;
		recoveryTimeEndEvent = this.time.delayedCall(2000, recoveryFinished, [], this);
		recoveryTimeEvent = this.time.delayedCall(200, recoveryTimeBlink, [false], this);
		this.add.image(vies*32, 0, 'coeur_vide').setOrigin(0, 0);
	}
}

function recoveryFinished() {
	recoveryTime = false;
}

function recoveryTimeBlink(isWhite) {
	if(recoveryTime && vies > 0) {
		if(isWhite) {
			player.setTint(0xffffff);
		} else {
			player.setTintFill(0xffffff);
		}
		recoveryTimeEvent = this.time.delayedCall(200, recoveryTimeBlink, [!isWhite], this);
	} else {
		player.setTint(0xffffff);
	}
}

function ajouterFantome() {
	posX = Math.floor((Math.random() * 15) + 1)*32+16;
	posY = Math.floor((Math.random() * 10) + 1)*32+16;
	while(posX > player.x-96 && posX < player.x+96) {
		posX = Math.floor((Math.random() * 15) + 1)*32+16;
	}
	while(posY > player.y-96 && posY < player.y+96) {
		posY = Math.floor((Math.random() * 10) + 1)*32+16;
	}
	var fantome = ghosts.create(posX, posY, 'mob');
	velX = Math.floor((Math.random() * 3)-1);
	velY = Math.floor((Math.random() * 3)-1);
	while(velX == 0 && velY == 0) {
		velX = Math.floor((Math.random() * 3)-1);
		velY = Math.floor((Math.random() * 3)-1);
	}
	fantome.setVelocity(160*velX, 160*velY);
}