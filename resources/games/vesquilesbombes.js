const config ={
	width: 800,
	height: 600,
	type: Phaser.AUTO,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {y: 450}
		}
	},
	scene: {
		preload: preload,
		create: create,
		update: update
	}
	
	
}



var game = new Phaser.Game(config);
let plateformes;
let joueur;
let cursors;
let fond;
let star;
var score=0;
var scoreText;
var bombs;
var gameOver;
var nbStar;
var temps;
var secondes;
var timer;
var total;


function preload(){
	this.load.image('fond' , '../resources/games/assets/vesquilesbombes/fond.jpg');
	this.load.spritesheet('joueur','../resources/games/assets/vesquilesbombes/joueur.png', {frameWidth: 32, frameHeight: 48});
	this.load.image('plateforme' , '../resources/games/assets/vesquilesbombes/plateforme.jpg');
	this.load.image('star' , '../resources/games/assets/vesquilesbombes/star.png');
	this.load.image('bomb' , '../resources/games/assets/vesquilesbombes/bomb.jpg');
}

function create(){

	score = 0;

	secondes = 0;

	temps = this.time.addEvent({ delay: 1000, callback: function seconde(){secondes++;}, callbackScope: this, loop: true });

	gameOver = false;
	nbStar = 0;
	this.physics.resume();
	
	this.add.image(400,300,'fond');
	plateformes = this.physics.add.staticGroup();
	plateformes.create(30,150,'plateforme');
	plateformes.create(180,350,'plateforme');
	plateformes.create(670,280,'plateforme');
	plateformes.create(500,450,'plateforme');
	plateformes.create(750,180,'plateforme');
	plateformes.create(400,568,'plateforme').setScale(2).refreshBody();
	
	
	joueur = this.physics.add.sprite(100, 450, 'joueur')
	joueur.body.collideWorldBounds = true;
	joueur.setBounce(0.2);
	this.physics.add.collider(joueur,plateformes);
	this.anims.create({
		key:'left',
		frames:
	this.anims.generateFrameNumbers('joueur',
	{ start: 0, end: 3 }),
			frameRate: 10,
			repeat: -1
	});
	
	this.anims.create({
		key:'right',
		frames:
	this.anims.generateFrameNumbers('joueur',
	{ start: 5, end: 8 }),
			frameRate: 10,
			repeat: -1
	});
	this.anims.create({
		key:'turn',
		frames: [{ key: 'joueur', frame: 4}],
		frameRate: 20
	});
	
	
	star = this.physics.add.group({
		key:'star',
		repeat: 30,
		setXY: {x:12, y:0, stepX:30}
	});
	this.physics.add.collider(star,plateformes);
	this.physics.add.overlap(joueur,star,collectStar,null,this);
	star.children.iterate(function(child){
		child.setBounceY(Phaser.Math.FloatBetween(0.2,0.6));
	});

	cursors = this.input.keyboard.createCursorKeys()
	scoreText = this.add.text(3, 1, 'Score: 0', { fontSize: '32px', fill:'#000'});
	timer = this.add.text(3, 30, 'Temps: 0', { fontSize: '32px', fill:'#000'});
	total = this.add.text(3, 65, '', { fontSize: '32px', fill:'#000'});
	bombs = this.physics.add.group();
	this.physics.add.collider(bombs,plateformes);
	
	this.physics.add.overlap(joueur, bombs, hitBomb, null, this);

	function hitBomb(joueur, bomb)
	{
		this.physics.pause();
		temps.remove(false);
		joueur.setTint(0xff0000);
		joueur.anims.play('turn');
		total.setText('Total: ' + ((score - 10*secondes)< 0 ? 0 : (score - 10*secondes)));
		appendScore(score,'score');
		gameOver = true;		
	}
	
}

function update(){
	joueur.setVelocityX(0);
	joueur.anims.play('turn');
	if(cursors.up.isDown){
		joueur.setVelocityY(-300)

	}
	if(cursors.right.isDown){
		joueur.setVelocityX(300)
		joueur.anims.play('right',true);
	}
	if(cursors.left.isDown){
		joueur.setVelocityX(-300)
		joueur.anims.play('left',true);
	}
	if(cursors.down.isDown){
		joueur.setVelocityY(500)

	}
	if(gameOver && cursors.space.isDown){
		this.scene.restart();
	}
	timer.setText('Temps: '+ secondes);
	
}
function collectStar(joueur,star){
	
	nbStar += 1;
	star.disableBody(true,true);
	score += 20;
	scoreText.setText('Score: '+ score);
	var x =(joueur.x<400) ?
	Phaser.Math.Between(400,800):
	Phaser.Math.Between(0,400);
		var bomb= bombs.create(x, 16,'bomb');
		bomb.setBounce(1);
	bomb.setCollideWorldBounds(true);
	bomb.setVelocity(Phaser.Math.Between(-200,200),20);
	bomb.allowGravity = false;	
	
}
