<?php

require_once "class/Jeu.class.php";
if(isset($_GET['id'])) {
    if(!empty($_GET['id'])) {
        $jeu = Jeu::createFromId($_GET['id']);
        $main = <<<HTML
        <div class="container">
            <div id="phaser" class="centered"></div>
        </div>
        <script src="resources/games/{$jeu->getJeu()}.js"></script>
HTML;
        echo $main;
    }
}