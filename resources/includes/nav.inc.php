<header>
    <!-- Navigation Collante -->

    <nav id="navigationCollante">
        <div class="logo">
            <a href="../index.php"><img src="../resources/img/logo.svg" alt="Goût Jeux"><p>Goût Jeux</p></a>
        </div>
        <div class="menu">
            <a href="../search.php">Jeux</a>
            <a href="../classements.php">Classements</a>
        </div>
        <div class="loggedOrNot">
            <?php include('resources/includes/loggedOrNot.inc.php');
            echo "\n";?>
        </div>
    </nav>

    <!-- Navigation Mobile -->

    <nav class="navigationMobile navigationFermee">
        <div class="navigationTitre">
            <div class="navigationTitreTextes">
                <a href="index.php"><img src="../resources/img/logo.png" alt="Goût Jeux"><p>Goût Jeux</p></a>
            </div>
            <div class="agregat">
                <div class="agregatLigne"></div>
                <div class="agregatLigne"></div>
                <div class="agregatLigne"></div>
            </div>
        </div>
        <div class="navigationLiens">
            <a href="/jeux">Jeux</a>
            <a href="/classements">Classements</a>
        </div>
    </nav>
</header>