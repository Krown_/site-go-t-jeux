<?php 

require_once('class/Message.class.php');
require_once('class/Utilisateur.class.php');

if(isset($_SESSION['identifiantGoûtJeux'])) {
    if(!empty($_SESSION['identifiantGoûtJeux'])) {
        $user = Utilisateur::createFromId($_SESSION['identifiantGoûtJeux']);
        $destinataire = Message::createFromDestinataire($_SESSION['identifiantGoûtJeux']);
        $expediteur = Message::createFromExpediteur($_SESSION['identifiantGoûtJeux']);
        $messagesReceived = "";
        foreach($destinataire as $messages) {
            $user2 = Utilisateur::createFromId($messages->getIdExp());
            $messagesReceived .= <<<HTML
            <div class="messages">
                <img src="{$user2->getPp()}" alt="Avatar" style="width:90px">
                <p><span>{$user2->getPseudo()}</span>le {$messages->getDateMess()}</p>
                <h4>{$messages->getObjet()}</h4>
                <h5>{$messages->getMessage()}</h5>
            </div>
HTML;
        }
        
        $messagesSent = "";
        foreach($expediteur as $messages) {
            $messagesSent .= <<<HTML
            <div class="messages">
                <img src="{$user->getPp()}" alt="Avatar" style="width:90px">
                <p><span>{$user->getPseudo()}</span>le {$messages->getDateMess()}</p>
                <h4>{$messages->getObjet()}</h4>
                <h5>{$messages->getMessage()}</h5>
            </div>
HTML;
        }
        $main = <<<HTML
        <div class="container messageMenu">
            <div class="tab">
                <button class="tablinks" onclick="openMessages(event, 'messagesSent')">Messages envoyés</button>
                <button class="tablinks" onclick="openMessages(event, 'messagesReceived')">Messages reçus</button>
            </div>

            <div id="messagesSent" class="tabcontent">
                $messagesSent
            </div>

            <div id="messagesReceived" class="tabcontent">
                $messagesReceived
            </div>
        </div>
HTML;
        echo $main;
    }
}
else {
    header('Location: ../../error.php');
    exit(); 
}