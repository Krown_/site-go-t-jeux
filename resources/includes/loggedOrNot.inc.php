<?php
require_once('class/Utilisateur.class.php');

session_start();

if(isset($_SESSION['identifiantGoûtJeux'])) {
    if(!empty($_SESSION['identifiantGoûtJeux'])) {
        $user = Utilisateur::createFromId($_SESSION['identifiantGoûtJeux']);
        $src = $user->getPp();
        $pseudo = $user->getPseudo();
        echo $logged = <<<HTML
<div class="user">
            <img src="$src" alt="Avatar" style="width:80px; height:80px;">
            <div class="dropdown">
                <button onclick="myFunction()" class="dropbutton btn btn-secondary"><i class="fas fa-angle-down"></i></button>
                <div id="myDropdown" class="dropdown-content">
                    <a href="../profil/$pseudo">Profil</a>
                    <a href="../messages.php">Messages</a>
                    <div class="dropdown-divider"></div>
                    <a href="../resources/scripts/logout.php">Se déconnecter</a>
                </div>
            </div>
        </div>
HTML;
    }
}
else {
    $user = null;
    echo $not = <<<HTML
<div class="login">
            <button type="button" class="btn btn-secondary btn-lg" data-toggle="modal" data-target="#myModal">Se connecter</button>

            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-dialog-centered">

                    <div class="modal-content">
                        <div class="verifLoginMail alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            Erreur : Aucun compte avec ce mail
                        </div>
                        <div class="modal-header">
                            <h1>Connexion</h1>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="/resources/scripts/login.php" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="loginEmail">Adresse e-mail</label>
                                    <input type="email" class="form-control" id="loginEmail" name="loginEmail" placeholder="Adresse e-mail" required>
                                </div>
                                <div class="form-group">
                                    <label for="InputPassword">Mot de passe</label>
                                    <input type="password" class="form-control" id="loginPassword" name="loginPassword" placeholder="Saisissez votre mot de passe" required>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="rememberMe">
                                    <label class="form-check-label" for="rememberMe">Se souvenir de moi</label>
                                </div>
                                <button type="submit" id="loginSubmit" class="btn btn-purple">Se connecter</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <a href="/register.php"><button type="button" class="btn btn-purple">S'enregistrer</button></a>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
HTML;
}
