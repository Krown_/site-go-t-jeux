<?php
require_once('class/Jeu.class.php');
if(isset($_GET['pseudo']) && ($user != null)) {
    if(!empty($_GET['pseudo'])) {
        $pseudo = htmlentities(trim($_GET['pseudo']));
        if($pseudo == $user->getPseudo()) {
            $tab = $user->getScoreTab();
            $scoreboard = "";
            if(sizeof($tab) > 0) {
                foreach($tab as $score) {
                    $jeu = Jeu::createFromId($score->getIdJeu());
                    $scoreboard .= <<<HTML
                    <div class="container classementJeuProfil">
                        <div class="card classementJeuProfilCard" style="width: 768px;">
                            <img src="{$jeu->getBanniereJeu()}" class="card-img-top" style="height: 256px;">
                                <div class="card-body">
                                    <div class="container">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td>{$jeu->getNom()}</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Nom d'utilisateur</td>
                                                    <td>{$score->getType()}</td>
                                                    <td>Date</td>
                                                </tr>
                                                <tr>
                                                    <td>{$user->getPseudo()}</td>
                                                    <td>{$score->getVal()}</td>
                                                    <td>{$score->getDate()}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                              </div>
                        </div>
                    </div>
HTML;
                }
            }
            else {
                $scoreboard = <<<HTML
                <h1 class="aucunePartie">Aucune partie de jeu n'a été effectuée pour le moment !</h1>
HTML;
            }
            if($user->getAfficherNomPrenom())
                $afficherNomPrenom = "checked";
            else
                $afficherNomPrenom = "";

            if($user->getAfficherAge())
                $afficherAge = "checked";
            else
                $afficherAge = "";
            $option = "";
            if($user->getSexe() == 'm') {
                for($i=1; $i<=5; $i++) {
                    $option .= "<option value=\"Avatar_$i\">Avatar $i</option>";
                }
            }
            else{
                for($i=6; $i<=9; $i++) {
                    $option .= "<option value=\"Avatar_$i\">Avatar $i</option>";
                }
            }

            $mainProfil = <<<HTML
            <section class="infoPerso">
                <div class="container">
                    <div class="row">
                        <div class="image">
                            <img src="{$user->getPp()}" class="testTest" alt="Avatar" style="width:200px; height:200px;">
                        </div>
                        <div class="nomPrenomPseudo">
                            <div>
                                <button type="button" id="modalProfil" class="btn btn-purple btn-lg" data-toggle="modal" data-target="#prefProfil">Réglages</button>
                            </div>
                            <div class="row">
                                <h4>{$user->getNom()}&nbsp;&nbsp;</h4>
                                <h4>{$user->getPrenom()}&nbsp;&nbsp;</h4>
                                <h4>{$user->getAge()} ans</h4>
                            </div>
                            <div class="row">
                                <h4>{$user->getPseudo()}</h4>
                            </div>
                            <div class="row">
                                <h4>{$user->getMail()}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="prefProfil" role="dialog">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="newMail alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    Erreur : Mail déjà utilisé 
                                </div>
                                <div class="newPseudo alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    Erreur : Pseudo déjà utilisé 
                                </div>
                                <div class="oldPassword alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    Erreur : Ancien mot de passe incorrect 
                                </div>
                                <div class="modal-header">
                                    <h1>Réglages</h1>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="/resources/scripts/modif.php" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="newEmail">Changement e-mail</label>
                                            <input type="email" class="form-control" id="newEmail" name="newEmail" placeholder="Adresse e-mail">
                                        </div>
                                        <div class="form-group">
                                            <label for="newPseudo">Changement pseudo</label>
                                            <input type="text" class="form-control" id="newPseudo" name="newPseudo" placeholder="Pseudo">
                                        </div>
                                        <div class="form-group">
                                            <label for="oldPassword">Ancien mot de passe</label>
                                            <input type="password" class="form-control" id="oldPassword" name="oldPassword" placeholder="Saisissez votre mot de passe">
                                        </div>
                                        <div class="form-group">
                                            <label for="newPassword">Nouveau mot de passe</label>
                                            <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="Saisissez votre mot de passe">
                                        </div>
                                        <div class="form-group">
                                            <label>Image de profil
                                              <select name="imgProfil">
                                                $option 
                                              </select>
                                            </label>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <h5>Afficher Nom et prénom</h5>
                                                <label class="switch">
                                                    <input type="checkbox" id="nomPrenom" name="nomPrenom" $afficherNomPrenom>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <h5>Afficher Age</h5>
                                                <label class="switch">
                                                    <input type="checkbox" id="showAge" name="showAge" $afficherAge>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <button type="submit" id="reglageSubmit" class="btn btn-purple">Confirmer</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="http://51.254.214.140/resources/img/space_profil.png" style="padding-top: 25px; padding-left: 50px;">
                </div>
            </section>
            <section class"classementPerso">
                $scoreboard
            </section>
HTML;
            echo $mainProfil;
        }
        else {            
            try {
                $player = Utilisateur::createFromPseudo($pseudo);
            } catch (PDOException $e) {
                header('Location: ../../error404.php');
                exit(); 
            }
            $playerTab = $player->getScoreTab();
            $scoreboard = "";
            if(sizeof($playerTab) > 0) {
                foreach($playerTab as $score) {
                    $jeu = Jeu::createFromId($score->getIdJeu());
                    $scoreboard .= <<<HTML
                    <div class="container classementJeuProfil">
                        <div class="card classementJeuProfilCard" style="width: 768px;">
                            <img src="{$jeu->getBanniereJeu()}" class="card-img-top" style="height: 256px;">
                                <div class="card-body">
                                    <div class="container">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td>{$jeu->getNom()}</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Nom d'utilisateur</td>
                                                    <td>{$score->getType()}</td>
                                                    <td>Date</td>
                                                </tr>
                                                <tr>
                                                    <td>{$player->getPseudo()}</td>
                                                    <td>{$score->getVal()}</td>
                                                    <td>{$score->getDate()}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                              </div>
                        </div>
                    </div>
HTML;
                }
            }
            else {
                $scoreboard = <<<HTML
                <h1 class="aucunePartie">Aucune partie de jeu n'a été effectuée pour le moment !</h1>
HTML;
            }
            if($player->getAfficherNomPrenom()) {
                $nom = $player->getNom();
                $prenom = $player->getPrenom();
                $mail = $player->getMail();
            }
            else {
                $nom = "*********";
                $prenom = "*********";
                $mail = "*********@gout-jeux.fr";
            }

            $age = ($player->getAfficherAge() == false ? "**" : $player->getAge());

            $mainProfil = <<<HTML
            <section class="infoPerso">
                <div class="container">
                    <div class="row">
                        <div class="image">
                            <img src="{$player->getPp()}" class="testTest" alt="Avatar" style="width:200px; height:200px;">
                        </div>
                        <div class="nomPrenomPseudo">
                            <div>
                                <button type="button" id="modalProfilMessage" class="btn btn-purple btn-lg" data-toggle="modal" data-target="#sendMessageProfil">Envoyer un message</button>
                            </div>
                            <div class="row">
                                <h4>$nom&nbsp;&nbsp;</h4>
                                <h4>$prenom&nbsp;&nbsp;</h4>
                                <h4>$age ans</h4>
                            </div>
                            <div class="row">
                                <h4>{$player->getPseudo()}</h4>
                            </div>
                            <div class="row">
                                <h4>$mail</h4>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="sendMessageProfil" role="dialog">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1>Envoyer un message</h1>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="/resources/scripts/sendMessage.php" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="destinatairePseudo">Pseudo du destinataire</label>
                                            <input type="text" class="form-control" readonly="true" id="destinatairePseudo" name="destinatairePseudo" placeholder="Pseudo du destinataire" value="{$player->getPseudo()}">
                                        </div>
                                        <div class="form-group">
                                            <label for="objetMessage">Objet</label>
                                            <input type="text" class="form-control" id="objetMessage" name="objetMessage" placeholder="Objet du message">
                                        </div>
                                        <div class="form-group">
                                            <label for="message">Message</label>
                                            <textarea class="form-control" rows="5" id="message" name="message"></textarea>
                                        </div>
                                        <button type="submit" id="sendMessageSubmit" class="btn btn-purple">Envoyer</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="http://51.254.214.140/resources/img/space_profil.png" style="padding-top: 25px; padding-left: 50px;">
                </div>
            </section>
            <section class"classementPerso">
                $scoreboard
            </section>
HTML;
            echo $mainProfil;
        }
    }
}
else {
    echo "<script type='text/javascript'>document.location.replace('error.php');</script>";
}
