<?php
require_once('../../class/Jeu.class.php');

$retour = "";

if(isset($_POST['filtres']) && is_array($_POST['filtres'])) {
	$typesJeux = array();
	foreach($_POST['filtres']as $k => $b) {
		if($b == 'true') {
			$typesJeux[] = $k;
		}
	}
	$res = Jeu::searchJeuFromType($typesJeux);
	foreach($res as $jeu) {
        $retour .= <<<HTML
        <div class="container filterGame">
                        <div class="card classementJeuProfilCard" style="width: 768px;">
                            <a href="jeu.php?id={$jeu->getId()}">
                            <img src="{$jeu->getBanniereJeu()}" class="card-img-top" style="height: 256px; width:768;">
                            </a>
                                <div class="card-body">
                                    <div class="container">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td>{$jeu->getDescription()}</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                              </div>
                        </div>
                    </div>
HTML;
	}
} else {
	$retour .= "<h1>La recherche n'a rien donné</h1>";
}
echo $retour;