<?php
session_start();

$_SESSION = array();
session_destroy();

setcookie('userGoûtJeux', '');

header('Location: ../../index.php');
exit();