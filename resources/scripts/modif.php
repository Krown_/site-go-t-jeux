<?php

require_once('../../class/Utilisateur.class.php');
session_start();
$user = Utilisateur::createFromId($_SESSION['identifiantGoûtJeux']);

if(isset($_POST['nomPrenom'])) {
    if(!empty($_POST['nomPrenom'])) {
        $user->setAfficherNomPrenom(1);
    }
}
else {
    $user->setAfficherNomPrenom(0);
}

if(isset($_POST['imgProfil'])) {
    if(!empty($_POST['imgProfil'])) {
        $img = (string) "http://51.254.214.140/resources/img/{$_POST['imgProfil']}.svg";
        $user->setPp($img);
    }
}
else {
    exit();
}

if(isset($_POST['showAge'])) {
    if(!empty($_POST['showAge'])) {
        $user->setAfficherAge(1);
    }
}
else {
    echo $_POST['showAge'];
    $user->setAfficherAge(0);
}

if(isset($_POST['newEmail'])) {
    if(!empty($_POST['newEmail'])) {
        $stmt = MyPDO::getInstance()->prepare("
            SELECT COUNT(idUt) as id
            FROM Utilisateur
            WHERE mailUt=?");
        $stmt->execute(array($_POST['newEmail']));
        $newMail = $stmt->fetch();
        if($newMail['id'] == 0)
            $user->setMail(htmlentities(trim($_POST['newEmail'])));
    }
}
else {
    exit();
}

if(isset($_POST['newPseudo'])) {
    if(!empty($_POST['newPseudo'])) {
        $stmt = MyPDO::getInstance()->prepare("
            SELECT COUNT(idUt) as id
            FROM Utilisateur
            WHERE pseudoUt=?");
        $stmt->execute(array($_POST['newPseudo']));
        $newPseudo = $stmt->fetch();
        if($newPseudo['id'] == 0)
            $user->setPseudo(htmlentities(trim($_POST['newPseudo'])));
    }
}
else {
    exit();
}


if(isset($_POST['newPassword']) && isset($_POST['oldPassword'])) {
    if(!empty($_POST['newPassword']) && !empty($_POST['oldPassword'])) {
        $stmt = MyPDO::getInstance()->prepare("
                        SELECT idUt, mdp
                        FROM Utilisateur
                        WHERE mailUt=?");
        $stmt->execute(array($user->getMail()));
        $newPassword = $stmt->fetch();
        
        $isPasswordCorrect = password_verify($_POST['oldPassword'], $newPassword['mdp']);
        if (!$newPassword) {
            header('Location: ../../error404.php');
            echo 'Mauvais identifiant ou mot de passe !'; //Afficher un message d'erreur;
            exit(); 
        }
        else {
            if ($isPasswordCorrect) {
                $user->setMdp(htmlentities(trim($_POST['newPassword'])));
                header('Location: ../scripts/logout.php');
                exit(); 
            }
            else {
                header('Location: ../../error404.php');
                echo 'Mauvais mot de passe !'; //Afficher un message d'erreur;
                exit(); 
            }
        }
    }
}
else {
    exit();
}

header("Location: ../../profil/{$user->getPseudo()}");
exit();