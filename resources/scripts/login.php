<?php

require_once('../../class/MyPDO.class.php');

if (isset($_POST['loginEmail']) && isset($_POST['loginPassword'])) {
    if (!empty($_POST['loginEmail']) && !empty($_POST['loginPassword'])) {

        $stmt = MyPDO::getInstance()->prepare("
                        SELECT idUt, mdp
                        FROM Utilisateur
                        WHERE mailUt=?");
        $stmt->execute(array($_POST['loginEmail']));
        $resultat = $stmt->fetch();

        $isPasswordCorrect = password_verify($_POST['loginPassword'], $resultat['mdp']);

        if (!$resultat) {
            header('Location: ../../index.php');
            echo 'Mauvais identifiant ou mot de passe !'; //Afficher un message d'érreur;
            exit(); 
        }
        else
        {
            if ($isPasswordCorrect) {
                session_start();
                $_SESSION['identifiantGoûtJeux'] = $resultat['idUt'];
                echo 'Vous êtes connecté !';
                header('Location: ../../index.php');
                exit(); 
            }
            else {
                header('Location: ../../index.php');
                echo 'Mauvais mot de passe !'; //Afficher un message d'érreur;
                exit(); 
            }
        }
    }
}