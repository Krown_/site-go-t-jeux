<?php

require_once('../../class/MyPDO.class.php');

if (isset($_POST['lastName']) && isset($_POST['firstName']) && isset($_POST['pseudo']) && isset($_POST['dateNais']) && isset($_POST['sexe']) && isset($_POST['email']) && isset($_POST['password_1']) && isset($_POST['password_2'])) {
    if (!empty($_POST['lastName']) && !empty($_POST['firstName']) && !empty($_POST['pseudo']) && !empty($_POST['dateNais']) && !empty($_POST['sexe']) && !empty($_POST['email']) && !empty($_POST['password_1']) && !empty($_POST['password_2'])) {
        $stmt = MyPDO::getInstance()->prepare("
                SELECT COUNT(idUt) as pseudo
                FROM Utilisateur
                WHERE pseudoUt=?");
        $stmt->execute(array($_POST['pseudo']));
        $pseudo = $stmt->fetch();

        $stmt = MyPDO::getInstance()->prepare("
            SELECT COUNT(idUt) as mail
            FROM Utilisateur
            WHERE mailUt=?");
        $stmt->execute(array($_POST['email']));
        $mail = $stmt->fetch();

        if (($mail['mail'] == 0) && ($pseudo['pseudo'] == 0) && ($_POST['password_1'] == $_POST['password_2'])) {
            $password = password_hash($_POST['password_1'],  PASSWORD_DEFAULT);
            $homme = 'http://51.254.214.140/resources/img/Avatar_1.svg';
            $femme = 'http://51.254.214.140/resources/img/Avatar_9.svg';
            $pp = ($_POST['sexe'] == 'm' ? $homme : $femme);
            $stmt = MyPDO::getInstance()->prepare("
                INSERT INTO Utilisateur(nomUt, prenomUt, pseudoUt, mailUt, mdp, sexeUt, dateNaissUt, ppUt) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->execute(array($_POST['lastName'], $_POST['firstName'], $_POST['pseudo'], $_POST['email'], $password, $_POST['sexe'], $_POST['dateNais'], $pp));
        }
    }
}

header('Location: ../../index.php');
exit(); 