<?php

require_once('../../class/Utilisateur.class.php');
session_start();


if(isset($_POST['destinatairePseudo']) && isset($_POST['objetMessage']) && isset($_POST['message'])) {
    if(!empty($_POST['destinatairePseudo']) && !empty($_POST['objetMessage']) && !empty($_POST['message'])) {
        $user = Utilisateur::createFromId($_SESSION['identifiantGoûtJeux']);
        $destinataire = Utilisateur::createFromPseudo($_POST['destinatairePseudo']);
        $user->sendMessage($destinataire, $_POST['objetMessage'], $_POST['message']);
        header("Location: ../../profil.php?pseudo={$destinataire->getPseudo()}");
        exit(); 
    }
}
else {
    header('Location: ../../error.php');
    exit(); 
}
