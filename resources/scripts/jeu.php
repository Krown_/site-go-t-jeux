<?php

require_once "class/Jeu.class.php";
if(isset($_GET['id']) && ($user != null)) {
    if(!empty($_GET['id'])) {
        $jeu = Jeu::createFromId($_GET['id']);
        if($_GET['id'] == 4) {
            $add = "<script src='../resources/games/phaser.min.js'></script>";
        }
        else
            $add = "";
        $main = <<<HTML
        <div class="container">
            <div id="test">
                <div id="phaser"></div>
            </div>
        </div>
        $add
        <script src="../resources/games/{$jeu->getJeu()}.js"></script>
		<script>
		function appendScore(score, typeScore) {
			$.ajax({
				url: 'resources/scripts/insertScore.php',
				type: 'post',
				data: {
					'idUt' : {$user->getId()},
					'idJeu' : {$_GET['id']},
					'valScore' : score,
					'intTpScore' : typeScore,
				},
				success: function(response){
				}
	});
		}
		</script>
HTML;
        echo $main;
    }
}
else {
    echo "<script type='text/javascript'>document.location.replace('error.php');</script>";

}