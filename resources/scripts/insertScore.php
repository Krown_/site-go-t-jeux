<?php
require_once('../../class/Utilisateur.class.php');

if(isset($_POST['idUt']) && $_POST['idJeu'] && $_POST['valScore'] && $_POST['intTpScore']) {
	$bdd = MyPDO::getInstance()->prepare("INSERT INTO Score
													  VALUES(:idUt,:idJeu,NOW(),:valScore,(SELECT idTpScore FROM TypeScore WHERE intTpScore = :intTpScore))");
	$bdd->execute(array("idUt" => $_POST['idUt'], "idJeu" => $_POST['idJeu'], "valScore" => $_POST['valScore'],"intTpScore" => $_POST['intTpScore']));
}