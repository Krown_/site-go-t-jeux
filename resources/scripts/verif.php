<?php

require_once('../../class/MyPDO.class.php');

if (isset($_POST['pseudo_check'])) {
  	$stmt = MyPDO::getInstance()->prepare("
            SELECT COUNT(idUt) as id
            FROM Utilisateur
            WHERE pseudoUt=?");
    $stmt->execute(array($_POST['pseudo']));
    $pseudo = $stmt->fetch();
    if ($pseudo['id'] > 0) {
  	  echo "taken";	
  	}else{
  	  echo 'not_taken';
  	}
  	exit();
}

if (isset($_POST['email_check'])) {
  	$stmt = MyPDO::getInstance()->prepare("
            SELECT COUNT(idUt) as id
            FROM Utilisateur
            WHERE mailUt=?");
    $stmt->execute(array($_POST['email']));
    $emailRegister = $stmt->fetch();
    if ($emailRegister['id'] > 0) {
  	  echo "taken";	
  	}else{
  	  echo 'not_taken';
  	}
  	exit();
}

if (isset($_POST['loginEmail_check'])) {
  	$stmt = MyPDO::getInstance()->prepare("
            SELECT COUNT(idUt) as id
            FROM Utilisateur
            WHERE mailUt=?");
    $stmt->execute(array($_POST['loginEmail']));
    $emailLogin = $stmt->fetch();
    if ($emailLogin['id'] == 0) {
  	  echo "not_taken";
  	}else{
  	  echo 'taken';
  	}
  	exit();
}

if (isset($_POST['newEmail_check'])) {
  	$stmt = MyPDO::getInstance()->prepare("
            SELECT COUNT(idUt) as id
            FROM Utilisateur
            WHERE mailUt=?");
    $stmt->execute(array($_POST['newEmail']));
    $newMail = $stmt->fetch();
    if ($newMail['id'] == 0) {
  	  echo "not_taken";
  	}else{
  	  echo 'taken';
  	}
  	exit();
}

if (isset($_POST['newPseudo_check'])) {
  	$stmt = MyPDO::getInstance()->prepare("
            SELECT COUNT(idUt) as id
            FROM Utilisateur
            WHERE pseudoUt=?");
    $stmt->execute(array($_POST['newPseudo']));
    $newPseudo = $stmt->fetch();
    if ($newPseudo['id'] == 0) {
  	  echo "not_taken";
  	}else{
  	  echo 'taken';
  	}
  	exit();
}