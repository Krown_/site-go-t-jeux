$(document).ready(function(){
    $('.verifMDP').hide();
    $('.verifUser').hide();
    $('.verifMail').hide();
    $('.verifLoginMail').hide();
    $('.verifLoginPassword').hide();
    $('.newMail').hide();
    $('.newPseudo').hide();
    $('.oldPassword').hide();
    var pseudo_state = false;
    var email_state = false;
    var loginEmail_state = false;
    var newEmail_state = false;
    var oldPassword_state = false;
    var newPseudo_state = false;
    $('#pseudo').on('blur', function(){
        var pseudo = $('#pseudo').val();
        if (pseudo == '') {
            pseudo_state = false;
            return;
        }
        $.ajax({
            url: 'resources/scripts/verif.php',
            type: 'post',
            data: {
                'pseudo_check' : 1,
                'pseudo' : pseudo,
            },
            success: function(response){
                if (response == 'taken' ) {
                    pseudo_state = false;
                    $('.verifUser').show();
                    $("#registerSubmit").attr("disabled", true);
                }else if (response == 'not_taken') {
                    pseudo_state = true;
                    $('.verifUser').hide();
                    $("#registerSubmit").attr("disabled", false);
                }
            }
        });
    });

    $('#email').on('blur', function(){
        var email = $('#email').val();
        if (email == '') {
            email_state = false;
            return;
        }
        $.ajax({
            url: 'resources/scripts/verif.php',
            type: 'post',
            data: {
                'email_check' : 1,
                'email' : email,
            },
            success: function(response){
                if (response == 'taken' ) {
                    email_state = false;
                    $('.verifMail').show();
                    $("#registerSubmit").attr("disabled", true);
                }else if (response == 'not_taken') {
                    email_state = true;
                    $('.verifMail').hide();
                    $("#registerSubmit").attr("disabled", false);
                }
            }
        });
    });

    $('#loginEmail').on('blur', function(){
        var loginEmail = $('#loginEmail').val();
        if (loginEmail == '') {
            loginEmail_state = false;
            return;
        }
        $.ajax({
            url: 'resources/scripts/verif.php',
            type: 'post',
            data: {
                'loginEmail_check' : 1,
                'loginEmail' : loginEmail,
            },
            success: function(response){
                if (response == 'not_taken' ) {
                    loginEmail_state = false;
                    $('.verifLoginMail').show();
                    $("#loginSubmit").attr("disabled", true);
                }else if (response == 'taken') {
                    loginEmail_state = true;
                    $('.verifLoginMail').hide();
                    $("#loginSubmit").attr("disabled", false);
                }
            }
        });
    });

    $('#newEmail').on('blur', function(){
        var newEmail = $('#newEmail').val();
        if (newEmail == '') {
            newEmail_state = false;
            return;
        }
        $.ajax({
            url: 'resources/scripts/verif.php',
            type: 'post',
            data: {
                'newEmail_check' : 1,
                'newEmail' : newEmail,
            },
            success: function(response){
                if (response == 'taken' ) {
                    newEmail_state = false;
                    $('.newMail').show();
                    $("#reglageSubmit").attr("disabled", true);
                }else if (response == 'not_taken') {
                    newEmail_state = true;
                    $('.newMail').hide();
                    $("#reglageSubmit").attr("disabled", false);
                }
            }
        });
    });

    $('#newPseudo').on('blur', function(){
        var newPseudo = $('#newPseudo').val();
        if (newPseudo == '') {
            newPseudo_state = false;
            return;
        }
        $.ajax({
            url: 'resources/scripts/verif.php',
            type: 'post',
            data: {
                'newPseudo_check' : 1,
                'newPseudo' : newPseudo,
            },
            success: function(response){
                if (response == 'taken' ) {
                    newPseudo_state = false;
                    $('.newPseudo').show();
                    $("#reglageSubmit").attr("disabled", true);
                }else if (response == 'not_taken') {
                    newPseudo_state = true;
                    $('.newPseudo').hide();
                    $("#reglageSubmit").attr("disabled", false);
                }
            }
        });
    });

});

function check_password(field_1, field_2){
    if(document.getElementById(field_1).value != document.getElementById(field_2).value){
        $('.verifMDP').show();
        return false;
    }
    else{
        $('.verifMDP').hide();
        return true;
    }
}

function openMessages(evt, tabContent) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabContent).style.display = "block";
    evt.currentTarget.className += " active";
}
