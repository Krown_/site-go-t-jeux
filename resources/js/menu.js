/* MENU MOBILE */

$(document).ready(function(){
	$('.navigationMobile').css('left','-350px');
	$('.agregat').on({
		click:function(){
			if ($('.navigationMobile').hasClass('navigationFermee')) {
				$('.navigationMobile').css('left','0px');
				$('.agregat').css({
					right: '20px',
					border: 'solid 2px #111'
				});
				$('.agregatLigne').css({
					'background-color': '#111'
				});
				$('.navigationMobile').removeClass('navigationFermee');
			} else {
				$('.navigationMobile').css('left','-350px');
				$('.agregat').css({
					right: '-50px',
					top: '20px',
					border: 'solid 2px #000'
				});
				$('.agregatLigne').css({
					'background-color': '#000'
				});
				$('.navigationMobile').addClass('navigationFermee');
			}		
		}
	})
	$('.navigationLiens a').on({
		click:function(){
			$('.navigationMobile').css('left','-350px');
			$('.agregat').css({
				right: '-50px',
				top: '20px',
				border: 'solid 2px #000'
			});
			$('.agregatLigne').css({
				'background-color': '#000'
			});
			$('.navigationMobile').addClass('navigationFermee');			
		}
	})
});

/* DropDown Button */

function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function(event) {
  if (!event.target.matches('.dropbutton')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}