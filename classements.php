<?php
require_once('class/MyPDO.class.php');
require_once('class/Jeu.class.php');
include('resources/includes/head.inc.php');?>
<body>
<?php include('resources/includes/nav.inc.php');
$page = "<section><div class=\"contentClassement classement\">";

$requeteIdJeux = MyPDO::getInstance()->prepare(<<<SQL
	SELECT idJeu FROM Jeu
SQL
);
$requetePseudo = MyPDO::getInstance()->prepare(<<<SQL
	SELECT pseudoUt FROM Utilisateur WHERE idUt = :idUt
SQL
);
$requeteIdJeux->execute();
$idJeux = $requeteIdJeux->fetchAll();
foreach($idJeux as $id) {
    $i = 1;
	$jeu = Jeu::createFromId($id['idJeu']);
	$typesScore = $jeu->getTypeScore();
	foreach($typesScore as $typeScore) {
		$scores = $jeu->getTabScore($typeScore);
		$page .= <<<HTML
		<div class="card classementCard" style="width: 768px;">
            <a href="../jeu.php?id={$jeu->getId()}">
            <img src="{$jeu->getBanniereJeu()}" class="card-img-top" style="height: 256px;">
            </a>
            <div class="card-body">
                <div class="container">
                    <table class="table table-hover">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <td>#</td>
                                <td>Nom d'utilisateur</td>
                                <td>$typeScore</td>
                                <td>Date</td>
                            </tr>
HTML;
		$users = array();
		foreach($scores as $score) {
			$requetePseudo->execute(array('idUt' => $score->getIdUt()));
			$pseudo = "";
			if (($nomJoueur = $requetePseudo->fetch()) !== false) {
				$pseudo = $nomJoueur['pseudoUt'];
			} else {
				$pseudo = 'JOUEUR NON IDENTIFIE';
			}
			if(!in_array($pseudo, $users)) {
				$page .= <<<HTML
							<tr>
                                <td>$i</td>
								<td><a href="../profil/$pseudo">$pseudo</td>
								<td>{$score->getVal()}</td>
								<td>{$score->getDate()}</td>
							</tr>
HTML;
				$users[] = $pseudo;
                $i++;
			}
		}
		
		$page .= <<<HTML
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
HTML;
	}
}
$page .= <<<HTML
    </div>
    </section>
    <footer class="footer">
        <p>© 2019 - Tous droits réservés - Goût Jeux</p>
    </footer>
</body>
</html>
HTML;
echo "\n$page";