<?php include('resources/includes/head.inc.php');?>

<body>
    <?php include('resources/includes/nav.inc.php');
    echo "\n";?>
    <section class="contentGame">
        <?php include('resources/scripts/jeu.php');?>
    </section>
    <footer class="footerGame">
        <p>© 2019 - Tous droits réservés - Goût Jeux</p>
    </footer>
</body>

</html> 