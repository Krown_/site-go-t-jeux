<?php include('resources/includes/head.inc.php');?>
<?php
require_once('class/MyPDO.class.php');

$req =  MyPDO::getInstance()->prepare(<<<SQL
	SELECT intTpJeu FROM TypeJeu
SQL
);
        $req->execute();
		$typeJeux = $req->fetchAll();
$js = <<<JS
var filtres = new Object();

function setList() {
	$.ajax({
		url: 'resources/scripts/searchGame.php',
		type: 'post',
		data: {
			'filtres' : filtres,
		},
		success: function(response){
			$("#listJeux").html(response);
		}
	});
}

$('.boutonFiltre').click(function() {
  $(this).toggleClass("active");
  filtres[$(this).text()] = !filtres[$(this).text()];
  setList()
});
JS;
$page = <<<HTML
<section class="content">
<div class="container listeJeux d-flex">
		<div class="flex-fill" id="listJeux">
		</div>
		<div>
			<h1 style="color:white;">Filtres :</h1>
			<div class="btn-group-vertical">
HTML;

foreach($typeJeux as $t) {
	$page .= '<button type="button" class="btn btn-outline-dark active boutonFiltre">'.$t['intTpJeu'].'</button>';
	$js .= 'filtres["'.$t['intTpJeu'].'"] = true;';
}
$page .= <<<HTML
			</div>
		</div>
	</div>
    </section>
    <footer class="footer">
        <p>© 2019 - Tous droits réservés - Goût Jeux</p>
    </footer>
	<script>
$js
setList();
	</script>
</body>

</html>
HTML;
?>
<body>
    <?php include('resources/includes/nav.inc.php');
    echo "\n$page";?>